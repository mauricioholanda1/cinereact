const BASE_URL = "https://api.themoviedb.org/3/";
module.exports = {
  URL: {
    BASE_URL: "https://api.themoviedb.org/3/",
    IMAGE_URL: "http://image.tmdb.org/t/p/w185",
    API_KEY: "api_key=2202c4bd282e2a75aa686a1ad67a9c89",
    SEARCH_QUERY: "search/movie?query=",
    PLACEHOLDER_IMAGE: "https://s3-ap-southeast-1.amazonaws.com/popcornsg/placeholder-movieimage.png"
  },
  Strings: {
    MAIN_TITLE: "Movie Database App",
    SECONDARY_TITLE: "Movie Details",
    PLACEHOLDER: "Enter search text",
    SEARCH_BUTTON: "Search",
    RELEASE_DATE: "Relase Date: ",
    LANGUAGE: "Language: ",
    POPULARITY: "Popularity: ",
    STATUS: "Status: ",
    RATINGS: "Ratings: ",
    POPULARITY : "Popularity: ",
    BUDGET: "Budget: ",
    REVENUE: "Revenue: ",
    RUNTIME: "Runtime: ",
    OVERVIEW: "Overview: ",
    MSG: "This field is required."
  },
  Colors: {
    Cyan: "#2196f3",
    Grey: "#EDEDED",
    Transparent: "transparent"
  }
};
