import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import AppStack from './appStack';
import AuthStack from './authStack';

const Routes = createAppContainer(
  createSwitchNavigator(
    {
      AuthStack,
      AppStack
    },
    {
      initialRouteName: 'AuthStack'
    }
  )
);

export default createAppContainer(Routes);
