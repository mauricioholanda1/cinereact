import {createAppContainer, createStackNavigator} from 'react-navigation';
import MainScreen from '../pages/MainScreen/MainScreen';
import SecondScreen from '../pages/SecondScreen/SecondScreen';

const AppStack = createStackNavigator(
  {
    MainScreen,
    SecondScreen,
  },
  {
    initialRouteName: 'MainScreen',
  },
);

export default AppStack;