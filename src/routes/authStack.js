import {createAppContainer, createStackNavigator} from 'react-navigation';
import Login from '../pages/login/Login';
import SignUp from '../pages/login/SignUp';

const AuthStack = createStackNavigator(
  {
    Login,
    SignUp,
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none'
  },
);

export default AuthStack;