import React from 'react';
import {StatusBar} from 'react-native';
import Routes from './routes';
import Constants from '../src/pages/components/utilities/Constants';

const App = () => (
  <>
    <StatusBar
      backgroundColor={Constants.Colors.Cyan}
      barStyle="light-content"
    />
    <Routes />
  </>
);
export default App;
